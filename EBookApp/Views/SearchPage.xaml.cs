﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using EBookApp.Models;
using Newtonsoft.Json;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace EBookApp.Views
{
    public partial class SearchPage : ContentPage
    {
        public SearchPage()
        {
            InitializeComponent();

            BindingContext = this;
        }

        public VolumeInfo BookDetail { get; set; }

        public ObservableCollection<VolumeInfo> BookList { get; set; } = new ObservableCollection<VolumeInfo>();

        public async void GetBook(string searchValue)
        {
            try
            {
                var httpClient = new HttpClient();
                var response = await httpClient.GetStringAsync($"https://www.googleapis.com/books/v1/volumes?q={searchValue}");
                var result = JsonConvert.DeserializeObject<Root>(response);

                BookList.Clear();
                foreach (var item in result.items)
                {
                    BookList.Add(item.volumeInfo);
                }
            }
            catch (Exception ex)
            {

            }
        }

        async void SearchBar_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(e.NewTextValue))
                {
                    return;
                }

                GetBook(e.NewTextValue);
            }
            catch (OperationCanceledException)
            {
                // Expected
            }
            catch (Exception ex)
            {

            }
        }

        public AsyncCommand<VolumeInfo> GoToBookDetailCommand => new AsyncCommand<VolumeInfo>(async (item) =>
        {
            BookDetail = item;
            await Navigation.PushAsync(new VolumeDetailPage
            {
                BindingContext = this
            });
        }, allowsMultipleExecutions: false);

        public AsyncCommand GoToReaderCommand => new AsyncCommand(async () =>
        {
            var Url = new Uri(BookDetail.previewLink);
            await Browser.OpenAsync(Url, BrowserLaunchMode.SystemPreferred);
        }, allowsMultipleExecutions: false);
    }
}
