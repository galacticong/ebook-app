﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EBookApp.Views
{
    public partial class BookDetailPage : ContentPage
    {
        public BookDetailPage()
        {
            InitializeComponent();
        }

        void BackTapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
