﻿using System;

namespace EBookApp.Models
{
    public class Items
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}
